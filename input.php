<!DOCTYPE html>
<html>
<head>
	<title>data penduduk</title>
	<link rel="stylesheet" type="text/css" href="style/dist/css/bootstrap.css">	
</head>
<body>
	<?php include_once("koneksi.php"); ?>
	<div style="margin-top: 5%" class="container">
		<form method="post" action="input.php">
			<h3>Input Provinsi</h3>
			<div class="form-group">
				<input type="text" name="nama_provinsi" autocomplete="off" placeholder="provinsi">
				<input type="submit" name="button" value="add">			
			</div>
		</form>

		<form method="post" action="input.php">
			<div>
				<h3>Input Kabupaten & jumlah penduduk</h3>
				<input type="text" name="nama_kabupaten" autocomplete="off" placeholder="kabupaten">
				<input type="numeric" name="jumlah_penduduk" autocomplete="off" placeholder="jumlah penduduk">
				<select name="provinsi">
					<?php 
					$getProvinsi = "SELECT * FROM provinsi";
					$provinsi = mysqli_fetch_all(mysqli_query($mysqli,$getProvinsi));					
					foreach ($provinsi as $p) { ?>
						<option value="<?= $p[0] ?>"><?= $p[1] ?></option>			 
					<?php } ?>
				</select>
				<input type="submit" name="submit" value="add">
			</div>
		</form>

		<table class="table table-border">
			<br>
			<tr>Data Provinsi</tr>

			<td>No</td>
			<td>Nama Provinsi</td>
			<td>Aksi</td>
			<?php
			$no = 1;
			$getProvinsi1 = "SELECT * FROM provinsi";
			$provinsi1 = mysqli_query($mysqli,$getProvinsi1);
			while($data = mysqli_fetch_array($provinsi1)) { ?>
				<tr>
					<td>
						<?php echo $no++ ?>
					</td>
					<td>
						<?php echo $data['nama_provinsi'] ?>
					</td>
					<td>
						<a  href="edit.php?id=<?php echo $data['id_provinsi']; ?>">Edit</a> 	
						<a  href="delete.php?id=<?php echo $data['id_provinsi']; ?>">Hapus</a>				
					</td>
				</tr>
				<?php
			}
			?>
		</table>
		<br>

		<form action="input.php" method="POST">
			<select name="filter_provinsi">
				<option value="kosong">Pilih Provinsi</option>
				<?php 
				$getProvinsi = "SELECT * FROM provinsi";
				$provinsi = mysqli_fetch_all(mysqli_query($mysqli,$getProvinsi));
				foreach ($provinsi as $p) { ?>
					<?php if ( $_POST['filter_provinsi']): ?>
					<option value="<?= $p[0] ?>" <?php if ($p[0]==$_POST['filter_provinsi']): ?>
						selected
					<?php endif ?>><?= $p[1] ?></option>			 	
					<?php else: ?> 
						<option value="<?= $p[0] ?>"><?= $p[1] ?></option>			 
					<?php endif ?>
					
				<?php } ?>
			</select>
			<input type="submit" name="filter" value="filter">
		</form>
		<?php if (isset($_POST['filter']) && $_POST['filter_provinsi']!=='kosong'): ?>
			<form action="cetak.php" method="GET"> 
				<input type="hidden" name="id_provinsi-cetak" value="<?=$_POST['filter_provinsi']?>">
				<button type="submit">Cetak</button>
			</form>
		<?php endif ?>

		<table class="table table-border">
			<tr>Data Kabupaten</tr>
			<td>No</td>
			<td>Nama Kabupaten</td>
			<td>Jumlah Penduduk</td>
			<td>Provinsi</td>
			<td style="text-align: center;" colspan="2">Aksi</td>
			<?php
			$no = 1;
			if (isset($_POST['filter'])) {
				if ($_POST['filter_provinsi']=='kosong') {
					$join = "SELECT kabupaten.id_provinsi,nama_kabupaten,jumlah_penduduk, provinsi.nama_provinsi,id_kabupaten
			FROM kabupaten
			INNER JOIN provinsi ON kabupaten.id_provinsi=provinsi.id_provinsi";
				}else{
				$join = "SELECT kabupaten.id_provinsi,nama_kabupaten,jumlah_penduduk, provinsi.nama_provinsi,id_kabupaten
					FROM kabupaten 
					INNER JOIN provinsi ON kabupaten.id_provinsi=provinsi.id_provinsi WHERE kabupaten.id_provinsi = ".$_POST['filter_provinsi']."" ;	
				}
			}else{
				$join = "SELECT kabupaten.id_provinsi,nama_kabupaten,jumlah_penduduk, provinsi.nama_provinsi,id_kabupaten
			FROM kabupaten
			INNER JOIN provinsi ON kabupaten.id_provinsi=provinsi.id_provinsi";
			}
			$kabupaten = mysqli_query($mysqli,$join);		
			while($data = mysqli_fetch_array($kabupaten)) {  ?>
				<tr>
					<td>
						<?php echo $no++ ?>
					</td>
					<td>
						<?php echo $data['nama_kabupaten'] ?>
					</td>
					<td>
						<?php echo $data['jumlah_penduduk'] ?>
					</td>	    		
					<td>
						<?php echo $data['nama_provinsi'] ?>
					</td>	    		
					<td>
						<a  href="edit1.php?id=<?php echo $data['id_kabupaten']; ?>">Edit</a> 	
						<a  href="delete1.php?id=<?php echo $data['id_kabupaten']; ?>">Hapus</a>				
					</td>
				</tr>	    	

				<?php
			}
			?>
		</table>		
	</div>


	<?php
	if(isset($_POST['button'])) {
		$nama_provinsi = $_POST['nama_provinsi'];

		$query = "INSERT INTO provinsi(nama_provinsi) VALUES('$nama_provinsi')";
		$result = mysqli_query($mysqli,$query);

		header('input.php','refresh');
	}
	?>		

	<?php
	if(isset($_POST['submit'])) {
		$nama_kabupaten = $_POST['nama_kabupaten'];
		$jumlah_penduduk = $_POST['jumlah_penduduk'];
		$id_provinsi = $_POST['provinsi'];


		$query2 ="INSERT INTO `kabupaten`(`nama_kabupaten`, `jumlah_penduduk`, `id_provinsi`) 
		VALUES ('$nama_kabupaten', '$jumlah_penduduk', $id_provinsi)";

		$result2 = mysqli_query($mysqli,$query2);

		// header('input.php');
	}
	?>

</body>
</html> 